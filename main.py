from http.server import BaseHTTPRequestHandler, HTTPServer
from http import HTTPStatus
from os import environ
import json
import pytz
from datetime import datetime

http_port = environ.get('PORT', '8080')
class handler(BaseHTTPRequestHandler):
    def do_GET(self):
        if self.path == '/health':
            self.send_response(HTTPStatus.OK.value)
            self.send_header('Content-type', 'application/json')
            self.end_headers()
            self.wfile.write(json.dumps({'status': '200'}).encode('utf-8'))
        elif self.path == '/':
            self.send_response(HTTPStatus.OK.value)
            self.send_header('Content-type','text/html')
            self.end_headers()
            tokyo = datetime.now(pytz.timezone('Asia/Tokyo')).strftime('%d.%m.%Y, %H:%M:%S')
            berlin = datetime.now(pytz.timezone('Europe/Berlin')).strftime('%d.%m.%Y, %H:%M:%S')
            newyork = datetime.now(pytz.timezone('America/New_York')).strftime('%d.%m.%Y, %H:%M:%S')
            message = """
            <html>
            <body>
            <div style="font-size: 1rem;"><b style="padding-right: 10px;">Tokyo:</b><span>{tokyo}</span></div>
            <div style="font-size: 1rem;"><b style="padding-right: 10px;">Berlin:</b><span>{berlin}</span></div>
            <div style="font-size: 1rem;"><b style="padding-right: 10px;">New York:</b><span>{newyork}</span></div>
            </body>
            </html>
            """.format(tokyo = tokyo, berlin = berlin, newyork = newyork)
            self.wfile.write(bytes(message, "utf8"))
        else:
            self.send_response(HTTPStatus.NOT_FOUND.value)

with HTTPServer(('', int(http_port)), handler) as server:
    server.serve_forever()
